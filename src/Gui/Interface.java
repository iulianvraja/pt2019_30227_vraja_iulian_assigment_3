package Gui;



import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import BussinesLogic.Insert;
import BussinesLogic.Delete;
import BussinesLogic.Update;
import ClientModel.Produs;
import DataAccesClasses.ClientBase;
import DataAccesClasses.ComandaBase;
import DataAccesClasses.ProdusBase;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Interface {
	private JFrame j;
public Interface() {
	j=new JFrame();
	j.setBounds(40,40,800,600);
	
	
	initialize();
}
public void initialize() {
	//operatii pe produs
	JPanel panel=new JPanel();
	panel.setVisible(true);
	panel.setLayout(null);
	
	JLabel jj1=new JLabel("Produs");
	jj1.setBounds(0, 0, 100, 40);
	JButton j1=new JButton("Insert");
	JLabel j5=new JLabel("stoc=");
	JLabel j2=new JLabel("id=");
	JLabel j3=new JLabel("pret=");
	JLabel j4=new JLabel("denumire=");
	JButton jj2=new JButton("Update");
	JButton jj3=new JButton("Delete");
	JButton jj4=new JButton("view");
	jj2.setBounds(0, 140, 100, 40);
	jj3.setBounds(0, 180, 100, 40);
	jj4.setBounds(0, 220, 100, 40);
	j1.setBounds(0, 50, 70, 40);
	j2.setBounds(160, 50, 40, 40);
	j3.setBounds(160, 90, 40, 40);
	j4.setBounds(160, 140, 40, 40);
	j5.setBounds(160,180,40,40);
	TextField tt1=new TextField();
	TextField tt2=new TextField();
	TextField tt3=new TextField();
	TextField tt4=new TextField();
	tt1.setBounds(110, 50, 40, 40);
	tt2.setBounds(110, 90, 40, 40);
	tt3.setBounds(110, 140, 40, 40);
	tt4.setBounds(110, 190, 40, 40);
	panel.add(jj2);
	panel.add(tt4);
	panel.add(jj4);
	panel.add(j5);
	panel.add(jj1);
	panel.add(j1);
	panel.add(j2);
	panel.add(j3);
	panel.add(j4);
	panel.add(tt1);
	panel.add(tt2);
	panel.add(tt3);
	panel.add(jj3);
	
	j1.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			int idProdus=Integer.parseInt(tt1.getText());
			int pret=Integer.parseInt(tt2.getText());
			String denumire=tt3.getText();
			int stoc=Integer.parseInt(tt4.getText());
			Insert.inserareProdus(idProdus, pret, denumire,stoc);
			
		}
		
		
	});
	
	jj2.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			int idProdus=Integer.parseInt(tt1.getText());
			int pret=Integer.parseInt(tt2.getText());
			String denumire=tt3.getText();
			int stoc=Integer.parseInt(tt4.getText());
			Update.updateProdus(idProdus, pret, denumire,stoc);
		}
		
		
	});
	jj3.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			int id=Integer.parseInt(tt1.getText());
		  Delete.deleteClient(id);
			
		}
		
		
	});
	jj4.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				ProdusBase.show();
			} catch (IllegalAccessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IllegalArgumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InvocationTargetException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}});
	//Client
	
	
	
	JLabel ll1=new JLabel("Client");
	ll1.setBounds(200, 0, 100, 40);
	JButton l1=new JButton("Insert");
	JLabel ll5=new JLabel("newid=");
	JLabel ll2=new JLabel("id=");
	JLabel ll3=new JLabel("email");
	JLabel ll6=new JLabel("prenume=");
	JLabel ll7=new JLabel("nume=");
	JLabel ll4=new JLabel("varsta");
	JButton l2=new JButton("Update");
	JButton l3=new JButton("Delete");
	JButton l4=new JButton("view");
	l1.setBounds(200, 50, 100, 40);
	l2.setBounds(200, 150, 100, 40);
	l3.setBounds(200, 200, 100, 40);
	l4.setBounds(200, 250, 100, 40);
	ll2.setBounds(310, 150, 70, 40);
	ll3.setBounds(310, 200, 70, 40);
	ll4.setBounds(310, 250, 70, 40);
	ll5.setBounds(310, 300, 70, 40);
	ll6.setBounds(310, 350, 70, 40);
	ll7.setBounds(310, 400, 70, 40);

	
	TextField tt11=new TextField();
	TextField tt22=new TextField();
	TextField tt33=new TextField();
	TextField tt44=new TextField();
	TextField tt55=new TextField();
	TextField tt66=new TextField();
	tt11.setBounds(380, 150, 40, 40);
	tt22.setBounds(380, 200, 40, 40);
	tt33.setBounds(380, 250, 40, 40);
	tt44.setBounds(380, 300, 40, 40);
	tt55.setBounds(380,350,40,40);
	tt66.setBounds(380,400,40,40);
	panel.add(l1);
	panel.add(l2);
	panel.add(l3);
	panel.add(ll1);
	panel.add(ll2);
	panel.add(ll3);
	panel.add(ll4);
	panel.add(ll5);
	panel.add(ll6);
	panel.add(ll7);
	panel.add(tt11);
	panel.add(tt22);
	panel.add(tt33);
	panel.add(tt44);
	panel.add(tt55);
	panel.add(tt66);
	panel.add(l4);
	l1.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			
			int varsta=Integer.parseInt(tt33.getText());
			String email=tt22.getText();
			String nume=tt55.getText();
			String prenume=tt66.getText();
			int id=Integer.parseInt(tt11.getText());
		   Insert.inserareClient(varsta, email, nume, prenume, id);
			
		}
		
		
	});
	
	l2.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			int varsta=Integer.parseInt(tt33.getText());
			String email=tt22.getText();
			String nume=tt55.getText();
			String prenume=tt66.getText();
			int id=Integer.parseInt(tt11.getText());
		   Update.updateClient(varsta, email, nume, prenume, id);
		}
		
		
	});
	l3.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			int id=Integer.parseInt(tt11.getText());
		  Delete.deleteClient(id);
			
		}
		
		
	});
	l4.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				ClientBase.show();
			} catch (IllegalAccessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IllegalArgumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InvocationTargetException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		
		
	});
	
	
	//Comanda
	JLabel xx1=new JLabel("Comanda");
	xx1.setBounds(500, 0, 100, 40);
	JButton x1=new JButton("Insert");
	JButton x4=new JButton("view");
	JLabel xx5=new JLabel("newid=");
	JLabel xx2=new JLabel("idComanda=");
	JLabel xx3=new JLabel("idClient");
	JLabel xx4=new JLabel("idProdus");
	JLabel xx6=new JLabel("cantitate");
	JButton x2=new JButton("Update");
	JButton x3=new JButton("Delete");
	x1.setBounds(500, 50, 100, 40);
	x2.setBounds(500, 150, 100, 40);
	x3.setBounds(500, 200, 100, 40);
	x4.setBounds(500, 250, 100, 40);
	xx2.setBounds(610, 150, 70, 40);
	xx3.setBounds(610, 200, 70, 40);
	xx4.setBounds(610, 250, 70, 40);
	xx5.setBounds(610, 300, 70, 40);
	xx6.setBounds(610, 350, 70, 40);
	

	
	TextField xx11=new TextField();
	TextField xx22=new TextField();
	TextField xx33=new TextField();
	TextField xx44=new TextField();
	TextField xx55=new TextField();
	
	x1.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			

			int idcomanda=Integer.parseInt(xx11.getText());
			int idclient=Integer.parseInt(xx22.getText());
		int idprodus=Integer.parseInt(xx33.getText());
			int cantitate=Integer.parseInt(xx55.getText());
			Insert.inserareComanda(idcomanda, idclient, idprodus, cantitate);
			Produs x=null;
			try {
				x = ProdusBase.getprodByid(idprodus);
			} catch (NumberFormatException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(x.getStock()-cantitate>0) {
			x.setStock(x.getStock()-cantitate);
			Update.updateProdus(x.getId(), x.getPret(), x.getDenumire(), x.getStock());}
			else {
				JOptionPane.showMessageDialog(panel, "Nu sunt suficiente produse in stoc", "Warning",
				        JOptionPane.WARNING_MESSAGE);}
	
			
			
		}
		
		
	});
	
	x2.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			int idcomanda=Integer.parseInt(xx11.getText());
			int idclient=Integer.parseInt(xx22.getText());
		int idprodus=Integer.parseInt(xx33.getText());
			int cantitate=Integer.parseInt(xx55.getText());
			Update.updateComanda(idcomanda, idclient, idprodus, cantitate);
			
		  
		}
		
		
	});
	x3.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			int id=Integer.parseInt(xx11.getText());
		  Delete.delete(id);
		  
			
		}
		
		
	});
	
	x4.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				ComandaBase.show();
			} catch (IllegalAccessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IllegalArgumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InvocationTargetException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		
		
	});
	
	
	xx11.setBounds(680, 150, 40, 40);
	xx22.setBounds(680, 200, 40, 40);
	xx33.setBounds(680, 250, 40, 40);
	xx44.setBounds(680, 300, 40, 40);
	xx55.setBounds(680,350,40,40);
	panel.add(xx55);
	panel.add(x1);
	panel.add(x2);
	panel.add(x3);
	panel.add(x4);
	panel.add(xx1);
	panel.add(xx2);
	panel.add(xx3);
	panel.add(xx4);
	panel.add(xx5);
	panel.add(xx6);
	panel.add(xx11);
	panel.add(xx22);
	panel.add(xx33);
	panel.add(xx44);
	
	j.setContentPane(panel);
	j.setVisible(true);
	
	
}
}
