package ClientModel;

public class Produs {
private int id;
private int pret;
private String denumire;
private  int stock;
public Produs(int i,int p,String d,int s) {
	id=i;
	pret=p;
	denumire=d;
	stock=s;
}
public int getStock() {
	return stock;
}
public void setStock(int stock) {
	this.stock = stock;
}
public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}
public int getPret() {
	return pret;
}
public void setPret(int pret) {
	this.pret = pret;
}
public String getDenumire() {
	return denumire;
}
public void setDenumire(String denumire) {
	this.denumire = denumire;
}
@Override
public String toString() {
	return  id + " " + pret + " " + denumire + " " + stock;
}

}
