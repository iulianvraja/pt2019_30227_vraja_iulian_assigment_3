package ClientModel;

public class Client {
	private int id;
	private String prenume;
	private String nume;
	private int varsta;
	private String email;

public Client(int v,String e,String n,String p, int i) {
	varsta=v;
	email=e;
	nume=n;
	prenume=p;
	id=i;
	
}
public int getVarsta() {
	return varsta;
}
public void setVarsta(int varsta) {
	this.varsta = varsta;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getNume() {
	return nume;
}
public void setNume(String nume) {
	this.nume = nume;
}
public String getPrenume() {
	return prenume;
}
public void setPrenume(String prenume) {
	this.prenume = prenume;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}

public String toString() {
	
	return id+" "+nume+" "+prenume+" "+varsta+" "+email;
			
}


}
