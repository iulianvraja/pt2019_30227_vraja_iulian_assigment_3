package BussinesLogic;


import DataAccesClasses.ClientBase;
import DataAccesClasses.ComandaBase;
import DataAccesClasses.ProdusBase;

public class Update {
	public static void updateClient(int varsta,String email, String nume,String prenume,int id) {
		
		ClientBase.updateTable(id, nume, prenume, varsta, email);
	}

	public static void updateProdus(int id,int pret,String denumire,int Stoc) {
	
		ProdusBase.updateTable(id, pret, denumire,Stoc);
	}
	public static void updateComanda(int ic,int iclient,int iprodus,int cantitate) {
		
		ComandaBase.updateTable(ic,iclient,iprodus,cantitate);
	}
}
