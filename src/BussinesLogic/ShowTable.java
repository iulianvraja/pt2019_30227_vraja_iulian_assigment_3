package BussinesLogic;


import java.util.ArrayList;

import java.lang.reflect.Method; 
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Constructor;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import ClientModel.Client;

public class ShowTable {
	private ArrayList<String> s=new ArrayList<String>();
	public static void createTable(ArrayList<Object> c) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		Class cls = c.get(0).getClass();
		
		String column[];
		ArrayList<String> s=new ArrayList<String>();
		Method m=null;
		try {
			Method declaredMethod = cls.getDeclaredMethod("toString");
			m = declaredMethod;
			System.out.println(m.getName());
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(Object k:c) {
			
			s.add((String) m.invoke(k));
			System.out.println((String) m.invoke(k));
		
		}
		Field[] fields = cls.getDeclaredFields();
		int n=fields.length;
		System.out.println(n);
		column=new String[n];
		int i;
		for(i=0;i<n;i++) {
			column[i]=fields[i].getName();
			
		}
		TableView(s,column);
	}
	private static void TableView(ArrayList s, String column[]) {
		
		 JFrame f=new JFrame();
		     String data[][];
		     int i,j;
		     int n=column.length;
		     int m=s.size();
		     int p=0;
		     String[] linie=new String[n];
		     data=new String[m][n];
		     for(i=0;i<m;i++) {
		    	 linie=((String) s.get(i)).split(" ");
		    	
		    	 for(j=0;j<n;j++) {
		    		data[i][j]=linie[j]; 
		    		
		    	 }
		    	 
		     }
		     JTable jt=new JTable(data,column);    
		     jt.setBounds(30,40,200,300);          
		     JScrollPane sp=new JScrollPane(jt);    
		     f.add(sp);          
		     f.setSize(300,400);    
		     f.setVisible(true);   
		    
		      
		
	}
}
