package BussinesLogic;


import DataAccesClasses.ClientBase;
import DataAccesClasses.ComandaBase;
import DataAccesClasses.ProdusBase;

public class Delete {
	public static void deleteClient(int id) {
		
		ClientBase.deleteTable(id);
	}

	public static void deleteProdus(int id) {
		ProdusBase.deleteTable(id);
	}
	public static void delete(int id) {
		
		ComandaBase.deleteTable(id);
	}
}
