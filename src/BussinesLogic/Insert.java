package BussinesLogic;


import ClientModel.Client;
import ClientModel.Comanda;
import ClientModel.Produs;
import DataAccesClasses.ClientBase;
import DataAccesClasses.ComandaBase;
import DataAccesClasses.ProdusBase;

public class Insert {
public static void inserareClient(int varsta,String email, String nume,String prenume,int id) {
	Client x=new Client(varsta,email,nume,prenume,id);
	ClientBase.insertTable(x);
}

public static void inserareProdus(int id,int pret,String denumire,int stoc) {
	Produs x=new Produs(id,pret,denumire,stoc);
	ProdusBase.insertTable(x);
}
public static void inserareComanda(int co,int cl,int pr,int c) {
	Comanda x=new Comanda(co,cl,pr,c);
	ComandaBase.insertTable(x);
}
}
